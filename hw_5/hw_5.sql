-- Задание №1
-- Команда для поиска номера телефона по первым символам.

SELECT * FROM customer_info.contacts
WHERE phone ~ '7912';

-- Задание №2

SELECT * FROM product_info.producer pp
    LEFT JOIN product_info.products p on pp.id = p.producer_id;

SELECT * FROM product_info.products p
    LEFT JOIN product_info.producer pp on pp.id = p.producer_id;

/*Порядок соединений в SQL (в частности, при использовании операторов LEFT JOIN и INNER JOIN) может влиять на результат запроса.

**INNER JOIN:** Порядок не влияет на результат INNER JOIN. Это потому что INNER JOIN возвращает только те строки, в которых существует совпадение в обоих таблицах, без учета порядка, в котором указаны таблицы.

Другими словами, `tableA INNER JOIN tableB` даст те же результаты, что и `tableB INNER JOIN tableA`.

**LEFT JOIN:** Однако порядок существенно влияет на результат при использовании LEFT JOIN. Это потому, что запрос LEFT JOIN возвращает все строки из левой таблицы и соответствующие строки из правой таблицы. Если для какой-то строки в левой таблице отсутствует соответствие в правой таблице, результат будет NULL.

То есть, если вы делаете `tableA LEFT JOIN tableB`, вы получите все строки из `tableA`, а корреспондирующие строки из `tableB` будут NULL, если не будет никакого совпадения. Если вы поменяете местами эти таблицы, полученные результаты могут быть совершенно другими.

Вкратце: в INNER JOIN порядок не имеет значения, а в LEFT JOIN порядок имеет большое значение.
*/

-- Задание №3

-- Вариант с внесением простым данных
INSERT INTO product_info.products(title, description, category_id, producer_id)
VALUES ('Test', 'Test', 1, 1)
RETURNING title, description, category_id, producer_id;

-- Вариант в случае, если имеется конфликт по id
INSERT INTO product_info.products(title, description, category_id, producer_id)
VALUES ('Test', 'Test', 1, 1)
ON CONFLICT (id) DO UPDATE SET
                    title = excluded.title,
                    description = excluded.description,
                    category_id = excluded.category_id,
                    producer_id = excluded.producer_id
RETURNING title, description, category_id, producer_id;

-- Задание №4
UPDATE product_info.products pp
SET producer_id = p.id
FROM (SELECT id FROM producer WHERE name = 'Apple') p
WHERE pp.producer_id = p.id;

-- Задание №5
-- Здесь с учетои того какая бд на данный момент по другому не сделать
DELETE FROM product_info.price AS p
USING customer_info.purchase AS cp
WHERE p.price_id = cp.price_id AND p.value < 5000;

-- Задание со *

-- Копируем из таблицы в файл
COPY product_info.products TO '/home/russi/Учеба/courses/otus/DB_course/hw_5/test.csv' CSV DELIMITER ',';
-- Копируем из файла в таблицу
COPY product_info.products_2 FROM '/home/russi/Учеба/courses/otus/DB_course/hw_5/test.csv' CSV;
